//$count
db.fruits.aggregate([

        {$match: {supplier: "Red Farms Inc."}},
        {$count: "totalItemsRedFarms"}

])
        
        
db.fruits.aggregate([

        {$match: {price:{$gt:50}}},
        {$count: "priceGreaterThan50"}

])
        
        
//$avg
db.fruits.aggregate([

        {$match: {onSale: true}},
        {$group:{_id:"$supplier", avgPricePerSupplier:{$avg: "$price"}}}

])
        
        
//$max
db.fruits.aggregate([

        {$match: {onSale: true}},
        {$group:{_id:"$supplier", maxPricePerSupplier:{$max: "$price"}}}

])
        
//$min
db.fruits.aggregate([

        {$match: {onSale: true}},
        {$group:{_id:"$supplier", minPricePerSupplier:{$min: "$price"}}}

])